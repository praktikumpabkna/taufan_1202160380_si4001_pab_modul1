package com.example.taufan_1202160380_si4001_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView value, sisi1, sisi2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        value = findViewById(R.id.ValueTextView);
    }

    public void count(View view) {
        try {
            sisi1 = (TextView) findViewById(R.id.sisi1_num);
            sisi2 = (TextView) findViewById(R.id.sisi2_num);
            int a = Integer.valueOf(sisi1.getText().toString());
            int b = Integer.valueOf(sisi2.getText().toString());
            int c = a * b;
            if (value != null) {
                value.setText(Integer.toString(c));
            }
        } catch (Exception e) {
            Toast toast =Toast.makeText(this, "Error! "+e, Toast.LENGTH_LONG);
            toast.show();
        }

    }
}
